# Markdown Link Check

This projects hosts the [Lychee-powered](https://github.com/lycheeverse/lychee) Markdown link checker
used by the GitLab technical writing team.

## Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:` keyword:

```yaml
include:
  - component: gitlab.com/gitlab-org/technical-writing-group/markdown-link-check/lychee@<VERSION>
```

The `<VERSION>` can be a commit SHA, tag, or branch name like `main`.

### Rules

This component uses these [rules](https://docs.gitlab.com/ee/ci/jobs/job_control.html#specify-when-jobs-run-with-rules)
by default, which prevents duplicate pipelines, but allows the component to run in all other cases:

```yaml
rules:
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
    when: never
  - when: always
```

To customize the component's rules to match your project's configuration, use `include: rules`
when including the component in your pipeline. For example, to only include the Markdown Link Check
job in default branch pipelines and merge request pipelines:

```yaml
include:
  - component: gitlab-org/technical-writing-group/markdown-link-check@<VERSION>
    rules:
      - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```

### Inputs

| Input            | Default value                   | Description |
|------------------|---------------------------------|-------------|
| `job-stage`      | `test`                          | The stage where you want the job to be added. |
| `job-name`       | `Markdown-link-check`           | The name to give the job. |
| `docs-path`      | `doc`                           | The path to the markdown files to check. Globs supported. |
| `lychee-version` | `latest`                        | The Lychee version of the alpine-based docker image to use. |
| `options`        | `--offline --include-fragments` | The CLI options to pass. The default sets Lychee to only check references to local files (offline) and anchors (fragments). Set to `""` to check external links, but not anchors, though it can cause the job to run for a very long time depending on the number of external links. |

For example, to use this component with a specific Lychee version, a different path to the markdown files,
and a different stage:

```yaml
include:
  - component: gitlab-org/technical-writing-group/markdown-link-check@<VERSION>
    inputs:
      lychee-version: "0.14.3"
      docs-path: internal/docs/**/*.md
      job-stage: lint
```

### Variables

N/A

## Contribute

Please read about [CI/CD components and best practices](https://docs.gitlab.com/ee/ci/components).
