# Test file 1

The first test file.

- [Link to heading in same file](#heading-1).
- [Link to another file](test-file2.md).
- [Link to a heading in another file](test-file2.md#heading-2).
- [Link to an external site](https://docs.gitlab.com)

## Heading 1
